﻿namespace Componente
{
    partial class UserControlGatito
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.b2 = new System.Windows.Forms.Button();
            this.c2 = new System.Windows.Forms.Button();
            this.a1 = new System.Windows.Forms.Button();
            this.c3 = new System.Windows.Forms.Button();
            this.a3 = new System.Windows.Forms.Button();
            this.c1 = new System.Windows.Forms.Button();
            this.a2 = new System.Windows.Forms.Button();
            this.b1 = new System.Windows.Forms.Button();
            this.b3 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.b2);
            this.panel1.Controls.Add(this.c2);
            this.panel1.Controls.Add(this.a1);
            this.panel1.Controls.Add(this.c3);
            this.panel1.Controls.Add(this.a3);
            this.panel1.Controls.Add(this.c1);
            this.panel1.Controls.Add(this.a2);
            this.panel1.Controls.Add(this.b1);
            this.panel1.Controls.Add(this.b3);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(256, 234);
            this.panel1.TabIndex = 10;
            // 
            // b2
            // 
            this.b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2.Location = new System.Drawing.Point(87, 80);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(81, 73);
            this.b2.TabIndex = 5;
            this.b2.UseVisualStyleBackColor = true;
            // 
            // c2
            // 
            this.c2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2.Location = new System.Drawing.Point(87, 159);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(81, 73);
            this.c2.TabIndex = 8;
            this.c2.UseVisualStyleBackColor = true;
            // 
            // a1
            // 
            this.a1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a1.Location = new System.Drawing.Point(0, 1);
            this.a1.Name = "a1";
            this.a1.Size = new System.Drawing.Size(81, 73);
            this.a1.TabIndex = 0;
            this.a1.UseVisualStyleBackColor = true;
            // 
            // c3
            // 
            this.c3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3.Location = new System.Drawing.Point(174, 159);
            this.c3.Name = "c3";
            this.c3.Size = new System.Drawing.Size(81, 73);
            this.c3.TabIndex = 7;
            this.c3.UseVisualStyleBackColor = true;
            // 
            // a3
            // 
            this.a3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a3.Location = new System.Drawing.Point(174, 1);
            this.a3.Name = "a3";
            this.a3.Size = new System.Drawing.Size(81, 73);
            this.a3.TabIndex = 1;
            this.a3.UseVisualStyleBackColor = true;
            // 
            // c1
            // 
            this.c1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1.Location = new System.Drawing.Point(0, 159);
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(81, 73);
            this.c1.TabIndex = 6;
            this.c1.UseVisualStyleBackColor = true;
            // 
            // a2
            // 
            this.a2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a2.Location = new System.Drawing.Point(87, 1);
            this.a2.Name = "a2";
            this.a2.Size = new System.Drawing.Size(81, 73);
            this.a2.TabIndex = 2;
            this.a2.UseVisualStyleBackColor = true;
            // 
            // b1
            // 
            this.b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1.Location = new System.Drawing.Point(0, 80);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(81, 73);
            this.b1.TabIndex = 3;
            this.b1.UseVisualStyleBackColor = true;
            // 
            // b3
            // 
            this.b3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3.Location = new System.Drawing.Point(174, 80);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(81, 73);
            this.b3.TabIndex = 4;
            this.b3.UseVisualStyleBackColor = true;
            // 
            // UserControlGatito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "UserControlGatito";
            this.Size = new System.Drawing.Size(261, 238);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button c2;
        private System.Windows.Forms.Button a1;
        private System.Windows.Forms.Button c3;
        private System.Windows.Forms.Button a3;
        private System.Windows.Forms.Button c1;
        private System.Windows.Forms.Button a2;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b3;
    }
}
