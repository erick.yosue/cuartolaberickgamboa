﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EJugador : EPersona
    {
        public int victorias { get; set; }

        public EJugador() : base()
        {
        }

        public EJugador(int victorias, string nombre, string cedula) : base (cedula,nombre)
        {
            this.victorias = victorias;
        }
    }


}
