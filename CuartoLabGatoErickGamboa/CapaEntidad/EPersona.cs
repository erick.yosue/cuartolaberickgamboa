﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public abstract class EPersona
    {
        public string nombre { get; set; }
        public string cedula { get; set; }
        

        protected EPersona()
        {
        }

        protected EPersona(string cedula, string nombre)
        {
            this.cedula = cedula;
            this.nombre = nombre;
        }
    }
}
