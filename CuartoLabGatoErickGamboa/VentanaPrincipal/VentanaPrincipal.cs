﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidad;

namespace VentanaPrincipal
{
    public partial class VentanaPrincipal : Form
    {
        public VentanaPrincipal()
        {
            InitializeComponent();
        }
        EJugador jugadorX = new EJugador();
        EJugador jugadorO = new EJugador();
        Boolean cambio = true;
        int empate = 0;
        private void VentanaPrincipal_Load(object sender, EventArgs e)
        {
            desabilitarTodosLosBotones();
            btnVolverJugar.Visible = false;
            
        }

        private void ButtonsClick(object sender, EventArgs e)
        {
            Button boton = (Button)sender;
            if (cambio)
            {
                boton.Text = "X";
                LblTurno.Text = "Turno de : O";
            }
            else
            {
                boton.Text = "O";
                LblTurno.Text = "Turno de : X";
            }
            cambio = !cambio;
            boton.Enabled = false;
            ganador();
        }
        public void ganador()
        {
            empate++;
            if (a1.Text != "" && a1.Text.Equals(a2.Text) && a2.Text.Equals(a3.Text))
            {
                if (a3.Text.Equals("X"))
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorX.nombre ;
                    jugadorX.victorias += 1;
                    lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                    LblTurno.Visible = false;

                }
                else
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorO.nombre;
                    jugadorO.victorias += 1;
                    lblVicO.Text = "Victorias de " + jugadorO.nombre + ": " + Convert.ToString(jugadorO.victorias);
                    LblTurno.Visible = false;
                }
                
                desabilitarTodosLosBotones();
                btnVolverJugar.Visible = true;
            }
            else if (b1.Text != "" && b1.Text.Equals(b2.Text) && b2.Text.Equals(b3.Text))
            {
                if (b3.Text.Equals("X"))
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorX.nombre;
                    jugadorX.victorias += 1;
                    lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                    LblTurno.Visible = false;

                }
                else
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorO.nombre;
                    jugadorO.victorias += 1;
                    lblVicO.Text = "Victorias de " + jugadorO.nombre + ": " + Convert.ToString(jugadorO.victorias);
                    LblTurno.Visible = false;
                }
                desabilitarTodosLosBotones();
                btnVolverJugar.Visible = true;
            }
            else if (c1.Text != "" && c1.Text.Equals(c2.Text) && c2.Text.Equals(c3.Text))
            {
                if (c3.Text.Equals("X"))
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorX.nombre;
                    jugadorX.victorias += 1;
                    lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                    LblTurno.Visible = false;

                }
                else
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorO.nombre;
                    jugadorO.victorias += 1;
                    lblVicO.Text = "Victorias de " + jugadorO.nombre + ": " + Convert.ToString(jugadorO.victorias);
                    LblTurno.Visible = false;
                }
                desabilitarTodosLosBotones();
                btnVolverJugar.Visible = true;
            }
            else if (a1.Text != "" && a1.Text.Equals(b1.Text) && b1.Text.Equals(c1.Text))
            {
                if (c1.Text.Equals("X"))
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorX.nombre;
                    jugadorX.victorias += 1;
                    lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                    LblTurno.Visible = false;

                }
                else
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorO.nombre;
                    jugadorO.victorias += 1;
                    lblVicO.Text = "Victorias de " + jugadorO.nombre + ": " + Convert.ToString(jugadorO.victorias);
                    LblTurno.Visible = false;
                }
                desabilitarTodosLosBotones();
                btnVolverJugar.Visible = true;
            }
            else if (a2.Text != "" && a2.Text.Equals(b2.Text) && b2.Text.Equals(c2.Text))
            {
                if (c2.Text.Equals("X"))
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorX.nombre;
                    jugadorX.victorias += 1;
                    lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                    LblTurno.Visible = false;

                }
                else
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorO.nombre;
                    jugadorO.victorias += 1;
                    lblVicO.Text = "Victorias de " + jugadorO.nombre + ": " + Convert.ToString(jugadorO.victorias);
                    LblTurno.Visible = false;
                }
                desabilitarTodosLosBotones();
                btnVolverJugar.Visible = true;
            }
            else if (a3.Text != "" && a3.Text.Equals(b3.Text) && b3.Text.Equals(c3.Text))
            {
                if (c3.Text.Equals("X"))
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorX.nombre;
                    jugadorX.victorias += 1;
                    lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                    LblTurno.Visible = false;

                }
                else
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorO.nombre;
                    jugadorO.victorias += 1;
                    lblVicO.Text = "Victorias de " + jugadorO.nombre + ": " + Convert.ToString(jugadorO.victorias);
                    LblTurno.Visible = false;
                }
                desabilitarTodosLosBotones();
                btnVolverJugar.Visible = true;
            }
            else if (a1.Text != "" && a1.Text.Equals(b2.Text) && b2.Text.Equals(c3.Text))
            {
                if (c3.Text.Equals("X"))
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorX.nombre;
                    jugadorX.victorias += 1;
                    lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                    LblTurno.Visible = false;

                }
                else
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorO.nombre;
                    jugadorO.victorias += 1;
                    lblVicO.Text = "Victorias de " + jugadorO.nombre + ": " + Convert.ToString(jugadorO.victorias);
                    LblTurno.Visible = false;
                }
                desabilitarTodosLosBotones();
                btnVolverJugar.Visible = true;
            }
            else if (a3.Text != "" && a3.Text.Equals(b2.Text) && b2.Text.Equals(c1.Text))
            {
                if (c1.Text.Equals("X"))
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorX.nombre;
                    jugadorX.victorias += 1;
                    lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                    LblTurno.Visible = false;


                }
                else
                {
                    lblMensaje.Text = "El ganador fue: " + jugadorO.nombre;
                    jugadorO.victorias += 1;
                    lblVicO.Text = "Victorias de " + jugadorO.nombre + ": " + Convert.ToString(jugadorO.victorias);
                    LblTurno.Visible = false;
                }
                desabilitarTodosLosBotones();
                btnVolverJugar.Visible = true;
            }
            else if (empate==9)
            {
                lblMensaje.Text = "¡¡Empate!!";
                btnVolverJugar.Visible = true;
            }
            


        }
        public void desabilitarTodosLosBotones() {
            a1.Enabled = false;
            a2.Enabled = false;
            a3.Enabled = false;
            b1.Enabled = false;
            b2.Enabled = false;
            b3.Enabled = false;
            c1.Enabled = false;
            c2.Enabled = false;
            c3.Enabled = false;

        }
        public void habilitarTodosLosBotones()
        {
            a1.Enabled = true;
            a2.Enabled = true;
            a3.Enabled = true;
            b1.Enabled = true;
            b2.Enabled = true;
            b3.Enabled = true;
            c1.Enabled = true;
            c2.Enabled = true;
            c3.Enabled = true;

        }
        public void desabilitarRegistro()
        {
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            txtNombreX.Visible = false;
            txtNombreO.Visible = false;
            txtCedulaX.Visible = false;
            txtCedulaO.Visible = false;

        }
        public void LimpiarTodosLosBotones()
        {
            a1.Text = "";
            a2.Text = "";
            a3.Text = "";
            b1.Text = "";
            b2.Text = "";
            b3.Text = "";
            c1.Text = "";
            c2.Text = "";
            c3.Text = "";

        }

        private void btnJugar_Click(object sender, EventArgs e)
        {
            if (txtNombreO.Text != "" && txtCedulaO.Text != "" && txtNombreX.Text != "" && txtCedulaX.Text != "")
            {
                
                jugadorX.nombre = txtNombreX.Text;
                jugadorX.cedula = txtCedulaX.Text;
                jugadorX.victorias = 0;

                
                jugadorO.nombre = txtNombreO.Text;
                jugadorO.cedula = txtCedulaO.Text;
                jugadorO.victorias = 0;

                habilitarTodosLosBotones();
                LblTurno.Text = "Turno de: X";
                lblMensa.Visible = false;
                desabilitarRegistro();
                btnJugar.Visible = false;
                lblVicX.Text = "Victorias de " + jugadorX.nombre + ": " + Convert.ToString(jugadorX.victorias);
                lblVicO.Text = "Victorias de " + jugadorO.nombre +": "+ Convert.ToString(jugadorO.victorias);
            }
            else
            {
                lblMensa.Text = "¡Debe completar los espacios requeridos!";
            }
        }

        private void btnVolverJugar_Click(object sender, EventArgs e)
        {
            habilitarTodosLosBotones();
            LblTurno.Visible = true;
            LimpiarTodosLosBotones();
            empate = 0;
            lblMensaje.Text = "En espera del próximo ganador";
        }


    }
}
