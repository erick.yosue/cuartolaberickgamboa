﻿using System;

namespace VentanaPrincipal
{
    partial class VentanaPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.a1 = new System.Windows.Forms.Button();
            this.a3 = new System.Windows.Forms.Button();
            this.a2 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.b3 = new System.Windows.Forms.Button();
            this.b1 = new System.Windows.Forms.Button();
            this.c2 = new System.Windows.Forms.Button();
            this.c3 = new System.Windows.Forms.Button();
            this.c1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNombreX = new System.Windows.Forms.TextBox();
            this.txtNombreO = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnJugar = new System.Windows.Forms.Button();
            this.txtCedulaX = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCedulaO = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LblTurno = new System.Windows.Forms.Label();
            this.lblMensa = new System.Windows.Forms.Label();
            this.btnVolverJugar = new System.Windows.Forms.Button();
            this.lblVicX = new System.Windows.Forms.Label();
            this.lblVicO = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // a1
            // 
            this.a1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a1.Location = new System.Drawing.Point(0, 1);
            this.a1.Name = "a1";
            this.a1.Size = new System.Drawing.Size(81, 73);
            this.a1.TabIndex = 0;
            this.a1.UseVisualStyleBackColor = true;
            this.a1.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // a3
            // 
            this.a3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a3.Location = new System.Drawing.Point(174, 1);
            this.a3.Name = "a3";
            this.a3.Size = new System.Drawing.Size(81, 73);
            this.a3.TabIndex = 1;
            this.a3.UseVisualStyleBackColor = true;
            this.a3.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // a2
            // 
            this.a2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a2.Location = new System.Drawing.Point(87, 1);
            this.a2.Name = "a2";
            this.a2.Size = new System.Drawing.Size(81, 73);
            this.a2.TabIndex = 2;
            this.a2.UseVisualStyleBackColor = true;
            this.a2.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // b2
            // 
            this.b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2.Location = new System.Drawing.Point(87, 80);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(81, 73);
            this.b2.TabIndex = 5;
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // b3
            // 
            this.b3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3.Location = new System.Drawing.Point(174, 80);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(81, 73);
            this.b3.TabIndex = 4;
            this.b3.UseVisualStyleBackColor = true;
            this.b3.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // b1
            // 
            this.b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1.Location = new System.Drawing.Point(0, 80);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(81, 73);
            this.b1.TabIndex = 3;
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // c2
            // 
            this.c2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2.Location = new System.Drawing.Point(87, 159);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(81, 73);
            this.c2.TabIndex = 8;
            this.c2.UseVisualStyleBackColor = true;
            this.c2.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // c3
            // 
            this.c3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3.Location = new System.Drawing.Point(174, 159);
            this.c3.Name = "c3";
            this.c3.Size = new System.Drawing.Size(81, 73);
            this.c3.TabIndex = 7;
            this.c3.UseVisualStyleBackColor = true;
            this.c3.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // c1
            // 
            this.c1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1.Location = new System.Drawing.Point(0, 159);
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(81, 73);
            this.c1.TabIndex = 6;
            this.c1.UseVisualStyleBackColor = true;
            this.c1.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.b2);
            this.panel1.Controls.Add(this.c2);
            this.panel1.Controls.Add(this.a1);
            this.panel1.Controls.Add(this.c3);
            this.panel1.Controls.Add(this.a3);
            this.panel1.Controls.Add(this.c1);
            this.panel1.Controls.Add(this.a2);
            this.panel1.Controls.Add(this.b1);
            this.panel1.Controls.Add(this.b3);
            this.panel1.Location = new System.Drawing.Point(219, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(256, 234);
            this.panel1.TabIndex = 9;
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.Location = new System.Drawing.Point(6, 171);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(0, 15);
            this.lblMensaje.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nombre del jugador X";
            // 
            // txtNombreX
            // 
            this.txtNombreX.Location = new System.Drawing.Point(161, 5);
            this.txtNombreX.Name = "txtNombreX";
            this.txtNombreX.Size = new System.Drawing.Size(100, 20);
            this.txtNombreX.TabIndex = 12;
            // 
            // txtNombreO
            // 
            this.txtNombreO.Location = new System.Drawing.Point(585, 5);
            this.txtNombreO.Name = "txtNombreO";
            this.txtNombreO.Size = new System.Drawing.Size(100, 20);
            this.txtNombreO.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(430, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Nombre del jugador O";
            // 
            // btnJugar
            // 
            this.btnJugar.Location = new System.Drawing.Point(311, 12);
            this.btnJugar.Name = "btnJugar";
            this.btnJugar.Size = new System.Drawing.Size(81, 48);
            this.btnJugar.TabIndex = 15;
            this.btnJugar.Text = "Jugar";
            this.btnJugar.UseVisualStyleBackColor = true;
            this.btnJugar.Click += new System.EventHandler(this.btnJugar_Click);
            // 
            // txtCedulaX
            // 
            this.txtCedulaX.Location = new System.Drawing.Point(161, 44);
            this.txtCedulaX.Name = "txtCedulaX";
            this.txtCedulaX.Size = new System.Drawing.Size(100, 20);
            this.txtCedulaX.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Cedula del jugador X";
            // 
            // txtCedulaO
            // 
            this.txtCedulaO.Location = new System.Drawing.Point(585, 44);
            this.txtCedulaO.Name = "txtCedulaO";
            this.txtCedulaO.Size = new System.Drawing.Size(100, 20);
            this.txtCedulaO.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(434, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Cedula del jugador O";
            // 
            // LblTurno
            // 
            this.LblTurno.AutoSize = true;
            this.LblTurno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTurno.Location = new System.Drawing.Point(6, 134);
            this.LblTurno.Name = "LblTurno";
            this.LblTurno.Size = new System.Drawing.Size(0, 15);
            this.LblTurno.TabIndex = 20;
            // 
            // lblMensa
            // 
            this.lblMensa.AutoSize = true;
            this.lblMensa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensa.Location = new System.Drawing.Point(194, 313);
            this.lblMensa.Name = "lblMensa";
            this.lblMensa.Size = new System.Drawing.Size(0, 18);
            this.lblMensa.TabIndex = 21;
            // 
            // btnVolverJugar
            // 
            this.btnVolverJugar.Location = new System.Drawing.Point(571, 259);
            this.btnVolverJugar.Name = "btnVolverJugar";
            this.btnVolverJugar.Size = new System.Drawing.Size(81, 48);
            this.btnVolverJugar.TabIndex = 22;
            this.btnVolverJugar.Text = "Volver a jugar";
            this.btnVolverJugar.UseVisualStyleBackColor = true;
            this.btnVolverJugar.Click += new System.EventHandler(this.btnVolverJugar_Click);
            // 
            // lblVicX
            // 
            this.lblVicX.AutoSize = true;
            this.lblVicX.Location = new System.Drawing.Point(35, 76);
            this.lblVicX.Name = "lblVicX";
            this.lblVicX.Size = new System.Drawing.Size(0, 13);
            this.lblVicX.TabIndex = 23;
            // 
            // lblVicO
            // 
            this.lblVicO.AutoSize = true;
            this.lblVicO.Location = new System.Drawing.Point(529, 76);
            this.lblVicO.Name = "lblVicO";
            this.lblVicO.Size = new System.Drawing.Size(0, 13);
            this.lblVicO.TabIndex = 24;
            // 
            // VentanaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 340);
            this.Controls.Add(this.lblVicO);
            this.Controls.Add(this.lblVicX);
            this.Controls.Add(this.btnVolverJugar);
            this.Controls.Add(this.lblMensa);
            this.Controls.Add(this.LblTurno);
            this.Controls.Add(this.txtCedulaO);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCedulaX);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnJugar);
            this.Controls.Add(this.txtNombreO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNombreX);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.panel1);
            this.Name = "VentanaPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gato";
            this.Load += new System.EventHandler(this.VentanaPrincipal_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void userControlGatito_Load(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Button a1;
        private System.Windows.Forms.Button a3;
        private System.Windows.Forms.Button a2;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button c2;
        private System.Windows.Forms.Button c3;
        private System.Windows.Forms.Button c1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNombreX;
        private System.Windows.Forms.TextBox txtNombreO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnJugar;
        private System.Windows.Forms.TextBox txtCedulaX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCedulaO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LblTurno;
        private System.Windows.Forms.Label lblMensa;
        private System.Windows.Forms.Button btnVolverJugar;
        private System.Windows.Forms.Label lblVicX;
        private System.Windows.Forms.Label lblVicO;
    }
}

